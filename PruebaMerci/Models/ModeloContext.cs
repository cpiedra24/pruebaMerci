using Microsoft.EntityFrameworkCore;

namespace PruebaMerci.Models
{
    public class ModeloContext : DbContext
    {
        public ModeloContext(DbContextOptions<ModeloContext> options)
            : base(options)
        {
        }
        public DbSet<clientes> clientes { get; set; }
        public DbSet<usuarios> usuarios { get; set; }
        public DbSet<contactos> contactos { get; set; }
    }
}