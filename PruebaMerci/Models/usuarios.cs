using System;

namespace PruebaMerci.Models
{
    public class usuarios
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string usuario { get; set; }
        public string contrasena { get; set; }
        public string tipoUsuario { get; set; }
    }
}